#ifndef MOVECONSTRUCTOR_H
#define MOVECONSTRUCTOR_H

#include <iostream>

namespace moveconstructor {

struct ArrayInt {
    ArrayInt() = default;
    ~ArrayInt() {delete [] data;}
    ArrayInt(size_t i);
    ArrayInt(const ArrayInt& other);
    ArrayInt(ArrayInt&& other);

    ArrayInt operator+(const ArrayInt& other);

    size_t size = 0;
    int* data = nullptr;
};

ArrayInt::ArrayInt(size_t i) : size(i), data(new int[i]) {
    std::cout << "ArrayInt(size_t i)" << std::endl;
}

ArrayInt::ArrayInt(const ArrayInt& other) : size(other.size), data(new int[size]) {
    std::cout << "ArrayInt(const ArrayInt& other)" << std::endl;
    for (size_t i = 0; i < size; ++i)
        data[i] = other.data[i];
}

ArrayInt::ArrayInt(ArrayInt &&other)
{
    std::cout << "ArrayInt(ArrayInt &&other)" << std::endl;
    data = other.data;
    other.data = nullptr;
}

ArrayInt ArrayInt::operator+(const ArrayInt& other) {
    if (other.size != size)
        return ArrayInt();
    ArrayInt result(size);
    for (size_t i = 0; i < size; ++i)
        result.data[i] = data[i] + other.data[i];
    return result;
}

void exampleOne() {
    ArrayInt arrayOne(5);
    //Код инициализации arrayOne
    ArrayInt arrayTwo(5);
    //Код инициализации arrayTwo
    ArrayInt arrayThree = arrayOne + arrayTwo;
    //Работаем с arrayThree
}


struct B {
    B() { std::cout << "B" << std::endl; }
    B(const B&) { std::cout << "B(const B&)" << std::endl; }
};

struct C {
    C() { std::cout << "C" << std::endl; }
    C(C&&) { std::cout << "C(C&&)" << std::endl; }
};

//У данного класса конструктор перемещения генерируется по умолчанию
struct DefaultMoveA {
    DefaultMoveA() { std::cout << "A" << std::endl; }

    B b;
    C c;
};

void exampleTwo() {
    //Просто вызов дефолтного конструктора
    DefaultMoveA objOne = DefaultMoveA();
    //Вызов конструктора перемещения. Вывод на экран: B(const B&) и C(C&&)
    DefaultMoveA objTwo = std::move(objOne);
}

//У данного класса конструктор перемещения задан явно
struct CustomMoveA {
    CustomMoveA() =default;
    CustomMoveA(CustomMoveA&&) {std::cout << "CustomMoveA(CustomMoveA&&)" << std::endl; }

    B b;
    C c;
};

void exampleThree() {
    CustomMoveA objOne;
    //Вызов конструктор копирования преведет к ошибку компиляции, т.к. в данном случаи он не сгенерирован по умолчанию
    //т.к. задан перемещающий конструктор
//    CustomMoveA objTwo = objOne;
    CustomMoveA objThree = std::move(objOne);
}


}

#endif // MOVECONSTRUCTOR_H
