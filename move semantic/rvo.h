#ifndef RVO_H
#define RVO_H

#include <iostream>

namespace rvo {

//Что почитать: https://pvs-studio.com/ru/blog/terms/6516/

struct A {
    A() { std::cout << "A" << std::endl; }
    A(A&&) { std::cout << "A(A&&)" << std::endl; }
};

//В данном случаи работает оптимизации компилятора RVO.
//Объект будет создан в памяти непосредственно в месте где сохраняется возвращаемое значение
A returnA() {
    return A();
}

void firstExample() {
    A a = returnA();
}

void nrvoExample() {
    std::string str;
    //...
    if (str.empty())
        return str;
    //...
    return str;
}

std::string withoutNrvoFirst(std::string val)
{
    //...
    //В данном случаи, nrvo не работает, поскольку данные под возвращаемое значение выделяет вызывающая функция,
    //а размещением объекта - вызываемая функция -> т.к. значение попадает из вне вызываемая функция не знает как разместить данные.
    return val;
}

std::string withoutNrvoSecond() {
    std::string str;
    //...
    if (str.empty())
        return std::string(); // RVO
    return str;
}

std::string withoutNrvoThird(bool flag) {
    std::string str1, str2;
    //...
    return flag ? str1 : str2;
}

}

#endif // RVO_H
