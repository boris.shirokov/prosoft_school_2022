#ifndef AUTOVSUNIQUE_H
#define AUTOVSUNIQUE_H

#include <iostream>
#include <memory>

namespace autovsunique {

struct A
{
    int a = 5;
};

/*!
 * \brief Очень простой аналог std::auto_ptr для типа А
 */
class NativeAutoPtr
{
public:
    explicit NativeAutoPtr(A* ptr) :
        m_ptr(ptr) {}
    NativeAutoPtr(NativeAutoPtr& other) {
        m_ptr = other.m_ptr;
        other.m_ptr = nullptr;
    }
    /*!
     * \brief TODO по аналогии с конструктором копирования следует реализовать оператор присваивания
     */
    // NativeAutoPtr(const NativeAutoPtr&)

    /*!
     * \brief Операторы * и -> являются интерфейсами реального указателя
     */
    A& operator*() const { return *m_ptr;}
    A* operator->() const { return m_ptr; }

private:
    /*!
     * \brief Запрещаем конструкторы копирования и операторы присваивания для создания из const объектов
     * const NativeAutoPtr obj; auto obj1 = obj;
     */
    NativeAutoPtr(const NativeAutoPtr&) = delete;
    NativeAutoPtr& operator=(const NativeAutoPtr&) = delete;

    A* m_ptr = nullptr;
};

void problemOfAutoPtr() {
    const auto foo = [](NativeAutoPtr ptr) {
        std::cout << ptr->a << std::endl;
    };
    NativeAutoPtr obj(new A);
    foo(obj);
    //В этой строчке падаем при разыменовании nullptr указателя, см. конструктор копирования NativeAutoPtr
    std::cout << obj->a << std::endl;
}

//Тоже, что и наш, но стандартный!
//void problemOfAutoPtr() {
//    const auto foo = [](std::auto_ptr<A> ptr) {
//        std::cout << ptr->a << std::endl;
//    };
//    std::auto_ptr<A> obj(new A);
//    foo(obj);
//    std::cout << obj->a << std::endl;
//}

void uniquePtrExample() {
    const auto foo = [](std::unique_ptr<A> ptr) {
        std::cout << ptr->a << std::endl;
    };
    std::unique_ptr<A> obj(new A);
    //Данный код, выдает ошибку компиляции: unique_ptr(const unique_ptr&) = delete
   // foo(obj);
    //Чтобы избежать данной ошибки, необходимо использовать std::move: foo(std::move(obj));
    foo(std::move(obj));
    std::cout << obj->a << std::endl;
}

}

#endif // AUTOVSUNIQUE_H
