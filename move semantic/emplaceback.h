#ifndef EMPLACEBACK_H
#define EMPLACEBACK_H

#include <iostream>
#include <vector>

namespace emplaceback {

struct A {
    A(const char*) {std::cout << "A(const char*)" << std::endl; }
    A() {std::cout << "A" << std::endl; }
    A(A&&) {std::cout << "A(A&&)" << std::endl; }
};

void example() {
    std::vector<A> vec;
    vec.reserve(2);
    //    vec.push_back(A());
    vec.emplace_back();
    vec.emplace_back("aaa");
}

}

#endif // EMPLACEBACK_H
