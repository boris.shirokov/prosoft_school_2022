#ifndef PASSVALUETOFUNCTION_H
#define PASSVALUETOFUNCTION_H

#include <string>

namespace passvaluetofunc {

//Хороший источник по теме: http://scrutator.me/post/2018/07/30/value_vs_reference.aspx

void asConstValue(const std::string) {}

void asValue(std::string) {}

void asConstLvalueRef(const std::string&){}

void asLvalueRef(std::string&){}

void asRvalueRef(std::string&&){}

//Не имеет смысла, поскольку у переданного объекта "забираем" указатель на данные и делаем его нулевым, т.е. пишем в указатель.
//void asConstRvalueRef(const std::string&&) {}

void example() {
    //Передаем не временный объект(можем взять адрес у данного объекта)

    std::string a = "abc";
    //Копируем lvalue объект, скопированный объект является константным
    asConstValue(a);
    //Копируем lvalue объект, скопированный объект не является константным
    asValue(a);
    //Передаем lvalue объект по lvalue ссылке, переданный объект является константным
    asConstLvalueRef(a);
    //Передаем lvalue объект по lvalue ссылке, переданный объект не является константным
    asLvalueRef(a);
    //Передаем lvalue объект по rvalue ссылке, ошибка компиляяции rvalue ссылка принимает только rvalue объект
    asRvalueRef(a);



    //Передаем rvalue объект, стоит отметить, что передаю const char* могу так сделать, поскольку
    // std::string имеет конструктор std::string(const char*), который не является explicit конструктором

    //Копирования не происходит, аргумент функции создается из const char*.
    //Созданный объект является константным
    asConstValue("abc");

    //Копирования не происходит, аргумент функции создается из const char*.
    asValue("abc");

    //Копирование не происходит, аргумент функции создается из const char*.
    //НЕОБХОДИМО ЗАПОМНИТЬ, что const T& продлевает время жизни rvalue объектов.
    //Однако, данное правило не работает для поля класса
    asConstLvalueRef("abs");

    //Ошибка компиляции, не констатная lvalue ссылка может принимать только не временные объекты.
    asLvalueRef("abc");

    //Передаем rvalue объект по rvalue ссылке
    asRvalueRef("abc");
}

void foo(std::string&& val) {
    const auto internalFoo = [](std::string&&) {};
    //Данный код преведет к ошибке компиляции, поскольку rvalue ссылка является lvalue значением
    internalFoo(val);
    //Чтобы данный код работал корректно необходимо воспользоваться функцией std::move
    internalFoo(std::move(val));
}

void moveExample() {
    std::string a = "12345";
    foo(std::move(a));
}


}

#endif // PASSVALUETOFUNCTION_H
