#ifndef EXPENSIVECOPY_H
#define EXPENSIVECOPY_H

#include <iostream>
#include <vector>

namespace expensivecopy {

struct ArrayInt {
    ArrayInt() = default;
    ~ArrayInt() {delete [] data;}
    ArrayInt(size_t i);
    ArrayInt(const ArrayInt& other);
//    ArrayInt(ArrayInt&& other);

    ArrayInt operator+(const ArrayInt& other);

    size_t size = 0;
    int* data = nullptr;
};

ArrayInt::ArrayInt(size_t i) : size(i), data(new int[i]) {
    std::cout << "ArrayInt(size_t i)" << std::endl;
}

ArrayInt::ArrayInt(const ArrayInt& other) : size(other.size), data(new int[size]) {
    std::cout << "ArrayInt(const ArrayInt& other)" << std::endl;
    for (size_t i = 0; i < size; ++i)
        data[i] = other.data[i];
}

//ArrayInt::ArrayInt(ArrayInt &&other)
//{
//    data = other.data;
//    other.data = nullptr;
//}

ArrayInt ArrayInt::operator+(const ArrayInt& other) {
    if (other.size != size)
        return ArrayInt();
    ArrayInt result(size);
    for (size_t i = 0; i < size; ++i)
        result.data[i] = data[i] + other.data[i];
    return result;
}

void example() {
    ArrayInt arrayOne(5);
    //Код инициализации arrayOne
    ArrayInt arrayTwo(5);
    //Код инициализации arrayTwo
    ArrayInt arrayThree = arrayOne + arrayTwo;
    //Работаем с arrayThree
}

}

#endif // EXPENSIVECOPY_H
