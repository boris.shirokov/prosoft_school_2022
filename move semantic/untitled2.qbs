import qbs

CppApplication {
    cpp.cxxFlags: "-o0"
    consoleApplication: true
    files: [
        "autovsunique.h",
        "expensivecopy.h",
        "emplaceback.h",
        "main.cpp",
        "moveconstructor.h",
        "passvaluetofunction.h",
        "rvalueandlvalue.h",
    ]

    Group {     // Properties for the produced executable
        fileTagsFilter: "application"
        qbs.install: true
        qbs.installDir: "bin"
    }
}
