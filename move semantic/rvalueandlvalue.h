#ifndef RVALUEANDLVALUE_H
#define RVALUEANDLVALUE_H

//Хороший источник: http://scrutator.me/post/2011/08/02/rvalue-refs.aspx

namespace rvalueandlvalue {

int& fooStatic() {
    static int a = 5;
    return a;
}

int fooStack() {
    int a = 5;
    return a;
}

struct A {
    int a = 5;
};

void example() {
    //Rvalue значение отличается от lvalue значения тем, что: у lvalue выражения можно получить адрес,
    //у rvalue выражения адрес получить нельзя
    int a = 0;
    //Переменная на стек, у которой можно получить адрес -> lvalue
    &a;
    //Временное значение -> rvalue
    &5;
    //Результат арифметической операции -> rvalue
    &(a + 5);
    //Переменная на стеке -> lvalue
    &(a += 5);
    //Функция возвращает ссылку на статическую переменную, стат. переменная имеет адрес -> lvalue
    &fooStatic();
    //Функция возвращает переменную на стеке -> rvalue
    &fooStack();
    A objA;
    //Ссылка на объект -> lvalue
    &objA;
    //Ссылка на временный объект -> rvalue
    &(A());
}

void example() {
    int a = 0;
    &a;
    &5;
    &(a + 5);
    &(a += 5);
    &fooStatic();
    &fooStack();
    A objA;
    &objA;
    &(A());
}

}

#endif // RVALUEANDLVALUE_H
